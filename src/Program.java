import cities.City;
import java.util.*;

public class Program {
    public static void main(String[] args) {
        ArrayList<City> cities = new ArrayList<>();
        cities.add(new City("Kalmar"));
        cities.add(new City("Vilhelmina"));
        cities.add(new City("Umeå"));
        cities.add(new City("Uppsala"));
        cities.add(new City("Växjö"));

        System.out.println("Cities I have lived in:");
        for (City c : cities) {
            System.out.println(c.name);
        }
    }
}
